# TinyBee README #

## About TinyBee ##

It's a new blog system, under development.

Currently translated in English and French, new languages ​​are expected for next versions.

## How to install TinyBee? ##

1. Download TinyBee files.
1. Copy folder to your web server.
1. Go to the root of TinyBee folder (ex: http://localhost/).
1. Fill the form with required information.

## Changelog ##

### v0.1.5 ###
* Bug fixes on Editor
* The appearance of button change if it markup is open
* Open/Close markup on cursor location

### v0.1.4 ###
* Formatting text with editor (or use our markup code)
* Insert pictures and links

### v0.1.3 ###
* Delete Category (and all post in the deleted category)
* Move post in another category with deleting category

### v0.1.2 ###
* Adding multiples CSS themes
* Choose theme from installer working now
* Change theme from admin panel
* Adding dutch langage (because I'm not a dutch speaker, it's possible to have many mistake in translation...)

### v0.1.1 ###
* Bug fixes

### v0.1.0 - Beta Release ###
* Add link to the Post
* Add Comment
* Add Comment moderation
* Add Langages for public blog (Based on selected Admin langage)
* Add news icons

### v0.0.5 ###
* Create users account
* modify users informations
* delete users
* Possibility to generate password for new user

### v0.0.4 ###
* Editing All post (except deleted post) is possible
* Editing blog Name
* It's possible to change your Logo
* And your Icon

### v0.0.3 ###
* Modified draft receive current date
* Limit post viewed (admin) by 10
* Button to delete Post

### v0.0.2 ###
* Added on Installation form:
    + Add Site Icon `.svg` or `.ico` or `.png`
    + Add Site Logo
* It's now possible to enabled/disabled category
* You can modify Post (Title, Content, draft status)
* Adding multiples icons

## About the autor ##
TinyBee is created by Loïc JACQUES.

[JL DigiDev](https://www.jldigidev.com)

[FunkyDuck](https://www.funkyduck.be)
