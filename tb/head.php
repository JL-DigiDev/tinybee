<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title><?= $blog["blogName"] ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/<?= $extIco ?>" href="<?= $ico ?>"/>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="scripts/bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" href="design/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="design/css/<?= $blog["blogTheme"] ?>.css">
    </head>

    <body>
