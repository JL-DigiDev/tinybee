
<footer class="container-fluid fixed-bottom">
    <div class="row bg-warning bg-one">
        <section class="col-md mx-5 my-2 text-left">
            Made with TinyBee <img src="design/bootstrap-icon/heart.svg" alt="Heart icon">.
        </section>

        <section class="col-md mx-5 my-2 text-right">
            <a href="?admin=c" class="text-dark text-two">Admin</a>
        </section>
    </div>
</footer>
</body>
</html>
