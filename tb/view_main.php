<main class="container-fluid">
    <div class="row">
        <section class="offset-md-1 col-md mx-5 mb-5 mt-1">
            <h1 class="alert alert-warning alert-one"><?= $title ?></h1>
                <?= $content ?>
            <div class="text-middle container-fluid">
                <div class="row justify-content-center">
                    <?= $btn ?>
                </div>
            </div>
        </section>
    </div>
</main>
