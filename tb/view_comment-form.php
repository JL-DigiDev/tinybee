<form class="alert alert-dark alert-two m-2 justify-content-center" action="?art=<?= $artId ?>" method="post">
    <div class="form-row">
        <label for="resName" class="col-md-1 m-1"><?= $cl["user"] ?></label> <input type="text" name="resName" value="" class="rounded col-md m-1" required>
    </div>

    <div class="form-row">
        <label for="resMail" class="col-md-1 m-1"><?= $cl["mail"] ?></label> <input type="email" name="resMail" value="" class="rounded col-md m-1" required>
    </div>

    <div class="form-row">
        <label for="resComment" class="col-md-1 m-1"><?= $cl["text"] ?></label>
        <textarea name="resComment" rows="8" class="rounded col-md m-1" required></textarea>
    </div>

    <div class="form-row justify-content-center">
        <button type="submit" name="send" class="btn btn-warning btn-one"><?= $cl["pub"] ?></button>
    </div>
</form>
