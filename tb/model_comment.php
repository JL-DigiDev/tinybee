<?php
    ob_start();
    if($actCmt==true && $genCmt!=0){
        $lg->setCmt();
        $cl=$lg->getCmt();

        if($_POST){
            $newComm=array();
            foreach($_POST as $key => $value) {
                $newComm[$key]=htmlentities($value);
            }
            $ncSql="INSERT INTO tb_comment(Name,Mail,IP,Comment,IsActive,ArtID) VALUES(:Name,:Mail,:IP,:Comment,:IsActive,:ArtID)";
            $ncArr=array("Name"=>$newComm["resName"],"Mail"=>$newComm["resMail"],"IP"=>$ip,"Comment"=>$newComm["resComment"],"IsActive"=>$genCmt,"ArtID"=>$artId);
            $sendCmt=new GetDataFromDb();
            $sendCmt->setSql($ncSql);
            $sendCmt->setData($ncArr);
            $sendCmt->sendData($db);
        }

        ?>
        <div class="alert alert-warning alert-one m-2">
            <?= $cl["title"] ?>
        </div>

        <?php require_once("view_comment-form.php");

        $gcSql="SELECT * FROM tb_comment WHERE ArtID=:ArtID AND IsActive=1";
        $gc=new GetDataFromDb();
        $gc->setSql($gcSql);
        $gc->setData(array("ArtID"=>$artId));
        $rCmt=$gc->getPreparedMultiQuery($db); ?>
            <div class="border border-secondary border-two rounded m-2 mb-5"><?php
                foreach ($rCmt as $key => $value) {
                    $cd=($key!=0)?"border-dark border-two border-top":""; ?>
                        <article class="<?= $cd ?> m-2 p-2">
                            <div class="alert alert-secondary alert-two">
                                <?= $value["Name"] ?> - <?= $value["Added"] ?>
                            </div>
                            <?= nl2br($value["Comment"]) ?>
                        </article>
                    <?php
                }
            ?></div>
        <?php
    }else{ ?>
        <div class="alert alert-danger alert-three m-2 font-weight-bold">
            <?= $bg["noCmt"] ?>
        </div>
    <?php }
    $comment=ob_get_clean();
