<?php
    $artId=htmlentities($_GET["art"]);
    $artSql="SELECT A.ID, A.Name Title, A.Added, A.Content, A.Comment, U.Name User FROM tb_art A JOIN tb_user U ON U.ID = A.UserID WHERE A.ID=:ID AND A.IsActive = 1";

    $art=new GetDataFromDb();
    $art->setSql($artSql);
    $art->setData(array("ID"=>$artId));
    $rArt=$art->getPreparedQuery($db);

    $aName=$rArt["Title"];
    $aAdded=$rArt["Added"];
    $aUser=$rArt["User"];
    $aContent=$rArt["Content"];
    $actCmt=($rArt["Comment"]==1)?true:false;
