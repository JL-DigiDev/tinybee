<?php
    $aSql="SELECT A.ID, A.Name, A.Content, A.Added, C.Name Cat, U.Name User FROM tb_art A JOIN tb_cat C ON A.CatID = C.ID JOIN tb_user U ON A.UserID = U.ID WHERE A.IsActive = 1 AND C.IsActive = 1";
    $acSql="SELECT COUNT(A.ID) CountArt FROM tb_art A JOIN tb_cat C ON A.CatID = C.ID WHERE A.IsActive = 1 AND C.IsActive = 1";
    $aMid=" AND A.CatID = :Id";
    $ga=new GetDataFromDb();
    $ca=new GetDataFromDb();

    if(is_numeric(@$_GET["lim"])){
        $lim=htmlentities($_GET["lim"]);
    }else{
        $lim=0;
    }

    $aEnd=" ORDER BY A.Added DESC LIMIT $lim, 5";

    if(@$_GET["cat"]){
        $rc=new GetDataFromDb();
        $ccSql=$cSql." AND ID =:Id";
        $ccArr=array("Id"=>htmlentities($_GET["cat"]));
        $rc->setSql($ccSql);
        $rc->setData($ccArr);
        $cData=$rc->getPreparedQuery($db);

        $title=$cData["Name"];

        $aSql.=$aMid.$aEnd;
        $acSql.=$aMid;

        $ga->setSql($aSql);
        $cArr=array("Id"=>htmlentities($_GET["cat"]));
        $ga->setData($cArr);
        $rArt=$ga->getPreparedMultiQuery($db);

        $ca->setSql($acSql);
        $ca->setData($cArr);
        $aCount=$ca->getPreparedQuery($db);
    }else{
        $title=$blog["blogName"];

        $aSql.=$aEnd;
        $ga->setSql($aSql);
        $rArt=$ga->getMultiQuery($db);

        $ca->setSql($acSql);
        $aCount=$ca->getQuery($db);
    }

    ob_start();
    foreach ($rArt as $key => $value) {
        $aName=$value["Name"];
        $aAdded=$value["Added"];
        $aUser=$value["User"];
        $aContent=$value["Content"];

        ?><a href="?art=<?= $value["ID"] ?>" class="nav-link text-dark"><?php require("view_art.php"); ?></a>

    <?php }
    $content=ob_get_clean();

    ob_start();
    $url="?";
    if(@$_GET["cat"]){
        $url.="cat=".htmlentities($_GET["cat"])."&amp;";
    }
    $url.="lim=";
    if($lim>0){ ?>
        <a href="<?= $url.($lim-5) ?>" class="btn btn-warning col-md-2 m-5"><?= $bg["prev"] ?></a>
    <?php }
    if($lim+5<$aCount["CountArt"]){ ?>
        <a href="<?= $url.($lim+5) ?>" class="btn btn-warning col-md-2 m-5"><?= $bg["next"] ?></a>
    <?php }
    $btn=ob_get_clean();
