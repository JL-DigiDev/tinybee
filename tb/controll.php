<?php
    // Blog data
    $dataBlog=new GetDataFromDb();
    $dataBlog->setSql();
    $blog=array();
    // Cat Data
    $cSql="SELECT * FROM tb_cat WHERE IsActive = 1";
    $dataCat=new GetDataFromDb();
    $dataCat->setSql($cSql);

    // Display Blog Data
    foreach ($dataBlog->getMultiQuery($db) as $key => $value) {
        $blog[$value["ID"]]=$value["Val"];
    }
    $ico=(@$blog["blogIcon"])?$blog["blogIcon"]:"design/TinyBee-Icon.svg";
    $extArr=explode(".",$ico);
    foreach ($extArr as $key => $value) {
        $extIco=$value;
    }
    $logo=(@$blog["blogLogo"])?$blog["blogLogo"]:"design/TinyBee-Icon.svg";
    $genCmt=$blog["blogComment"];

    // Lang
    switch ($blog["blogLang"]) {
        case "fr":
            $lg=new BlogFrench();
            break;
        case "en":
            $lg=new BlogEnglish();
            break;
        case "nl":
            $lg=new BlogDutch();
            break;
    }
    $lg->setGen();
    $bg=$lg->getGen();

    // Cat 2 Nav
    ob_start();
    foreach ($dataCat->getMultiQuery($db) as $key => $value) {
        ?><li class="nav-item mi"><a href="index.php?cat=<?= $value["ID"] ?>" class="nav-link text-white font-weight-bold"><?= $value["Name"] ?></a></li><?php
    }
    $nav=ob_get_clean();

    // Show page
    require_once("head.php");
    require_once("nav.php");
    if(@$_GET["art"]){
        require_once("view_art-start.php");
        require_once("model_art.php");
        require_once("view_art.php");
        require_once("model_comment.php");
        require_once("view_comment.php");
        require_once("view_art-end.php");
    }else if(@$_GET["cat"]){
        $title="";
        require_once("model_main.php");
        require_once("view_main.php");
    }else{
        require_once("model_main.php");
        require_once("view_main.php");
    }
    require_once("foot.php");
