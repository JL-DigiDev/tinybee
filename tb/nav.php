<nav class="navbar navbar-expand-sm bg-warning sticky-top bg-one">
    <span class="navbar-brand mb-0 h1"><a href="?"><img src="<?= $logo ?>" alt="TinyBee Icon" height="64px" ></a></span>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headNav" aria-controls="headNav" aria-expanded="false" aria-label="Toggle Navigation">
        <span class="navbar-toggler-icon">
            <img src="design/bootstrap-icon/grid.svg" alt="">
        </span>
    </button>

    <div class="collapse navbar-collapse offset-sm-1 col-sm-10 h-5 bg-warning bg-one" id="headNav">
        <ul class="navbar-nav mr-auto bg-warning bg-one">
            <?= @$nav ?>
        </ul>
    </div>
</nav>
