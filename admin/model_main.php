<?php
    $mainData=new GetDataFromDb();
    $mainData->setSql();
    $blogData=array();

    switch ($_SESSION["lang"]) {
        case 'fr':
            $textMain=new FrenchAdmin();
            $textCmn= new FrenchCommon();
            break;
        case 'en':
            $textMain=new EnglishAdmin();
            $textCmn=new FrenchCommon();
            break;
        case 'nl':
            $textMain=new DutchAdmin();
            $textCmn=new DutchCommon();
            break;
    }

    $textMain->setAdmin();
    $tm=$textMain->getAdmin();

    $textCmn->setCommon();
    $tc=$textCmn->getCommon();

    const FC="border border-warning rounded col-md mx-5 my-2 p-0";
    const FCH="bg-warning p-1";
    const FCI="rounded border border-dark";
    const FCB="btn btn-warning m-2";
    const FU="?admin=home";

    if(@$_POST["data"]){
        foreach ($_POST as $key => $value) {
            if($key!="data"){
                $id=$key;
                $val=$value;
            }
        }

        if(@$_FILES){
            foreach ($_FILES as $key => $value) {
                $pic=($key=="blogIcon")? new IconVerif(): new PicturesVerif();
                $pic->setPic($value);

                $dir="design/usr";
                $id=$key;
                $val=(@$value["name"]!="")?$dir."/".$value["name"]:"";
            }
            $pic->setDir($dir);
            $pic->loadPic();
        }

        $tSql="UPDATE tb_data SET Val=:Val WHERE ID=:ID";
        $tArr=array("Val"=>$val,"ID"=>$id);
        $pTitle=new GetDataFromDb();
        $pTitle->setSql($tSql);
        $pTitle->setData($tArr);
        $pTitle->sendData($db);
    }

    foreach ($mainData->getMultiQuery($db) as $key => $value) {
        $blogData[$value["ID"]]=$value["Val"];
    }

    $title=$tm["title"]." ".$blogData["blogName"];

    $cls="btn-secondary";
    $uri="admin={$_GET["admin"]}";

    $icon=(@$blogData["blogIcon"]!="")?$blogData["blogIcon"]:"design/TinyBee-Icon.svg";
    $logo=(@$blogData["blogLogo"]!="")?$blogData["blogLogo"]:"design/TinyBee-Icon.svg";

    ob_start();
    $tList=scandir("./design/css/");
    foreach ($tList as $key => $value) {
        if($key>1){
            $reg=explode(".",$value)[0];
            $name=explode("-",$reg)[1];
            $check=($reg==$blogData["blogTheme"])?"selected":"";
            ?>
                <option value="<?= $reg ?>" <?= $check ?>><?= $name ?></option>
            <?php
        }
    }
    $theme=ob_get_clean();

    ob_start();
    for($i=0;$i<3;$i++){
        $check=($i==$blogData["blogComment"])?"selected":"";
        switch($i){
            case 0:
                $opt=$tc["sta-dis"];
                break;
            case 1:
                $opt=$tc["sta-ena"];
                break;
            case 2:
                $opt=$tc["sta-pen"];
                break;
        }
        ?>
        <option value="<?= $i ?>" <?= $check ?>><?= $opt ?></option>
    <?php }
    $cmtOpt=ob_get_clean();
