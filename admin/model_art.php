<?php
    const FICO = "design/bootstrap-icon/";

    switch ($_SESSION["lang"]) {
        case 'fr':
            $textArt=new FrenchArt();
            break;
        case 'en':
            $textArt=new EnglishArt();
            break;
        case 'nl':
            $textArt=new DutchArt();
            break;
    }

    $textArt->setArt();
    $ta=$textArt->getArt();

    // Set limit to paginate art
    if(is_numeric(@$_GET["lim"])){
        $lim=htmlentities($_GET["lim"]);
    }else{
        $lim=0;
    }

    if(@$_POST){
        if(@$_POST["draft"]){
            $corr=new GetDataFromDb();
            $updSql="UPDATE tb_art SET Name=:Name,Content=:Content,Added=IF(IsActive=0,NOW(),Added),CatID=:CatID,Comment=:Comment,IsActive=:IsActive WHERE ID=:ID";
            $updArr=array("ID"=>"","Name"=>"","Content"=>"","CatID"=>"","Comment"=>1,"IsActive"=>1);
            foreach ($_POST as $key => $value) {
                if($key!="addArt" && $key!="draft" && $key!="lang"){
                    $updArr[$key]=htmlentities($value);
                }else if($key=="draft"){
                    $updArr["ID"]=htmlentities($value);
                }
            }
            $corr->setSql($updSql);
            $corr->setData($updArr);
            $corr->sendData($db);
        }else if(@$_POST["addArt"]){
            $send=new GetDataFromDb();
            $sSql="INSERT INTO tb_art(Name,Content,CatID,UserID,Comment,IsActive) VALUES(:Name,:Content,:CatID,:UserID,:Comment,:IsActive)";
            $pArt=array("Name"=>"","Content"=>"","CatID"=>"","UserID"=>htmlentities($_SESSION["UserID"]),"Comment"=>1,"IsActive"=>1);
            foreach ($_POST as $key => $value) {
                if($key!="addArt" && $key!="lang"){
                    $pArt[$key]=htmlentities($value);
                }
            }
            $send->setSql($sSql);
            $send->setData($pArt);
            $send->sendData($db);
        }
        if(@$_POST["edit"]){
            $dSql="SELECT * FROM tb_art WHERE ID = :id";
            $dArr=array("id"=>htmlentities($_POST["editId"]));

            $draft=new GetDataFromDb();
            $draft->setSql($dSql);
            $draft->setData($dArr);
            $e=$draft->getPreparedQuery($db);

            ob_start(); ?>
            <input type="hidden" name="draft" value="<?= $e["ID"] ?>">
            <?php
            $eHidden=ob_get_clean();

            $isDraft=($e["IsActive"]==0)?"checked":"";
        }
        if(@$_POST["del"]){
            $delSql="UPDATE tb_art SET IsActive = 2 WHERE ID = :ID";
            $delArr=array("ID"=>htmlentities($_POST["editId"]));

            $delRq=new GetDataFromDb();
            $delRq->setSql($delSql);
            $delRq->setData($delArr);
            $delRq->sendData($db);
        }
    }

    // getArticles
    $gArt=new GetDataFromDb();
    $aSql="SELECT A.ID ArtId, A.Name Title, A.Added, A.IsActive Active, A.Comment, C.Name Cat, U.Name User, (SELECT COUNT(*) FROM tb_comment WHERE ArtID=A.ID) Cmt, (SELECT COUNT(*) FROM tb_comment WHERE ArtID=A.ID AND IsActive=2) Pen FROM tb_art A JOIN tb_cat C ON A.CatId = C.ID JOIN tb_user U ON A.UserID = U.ID ORDER BY A.Added DESC LIMIT $lim, 10";
    $gArt->setSql($aSql);
    ob_start();
    foreach ($gArt->getMultiQuery($db) as $key => $value) {
        switch($value["Active"]){
            case 0:
                $c="dark";
                $bEdit="<button type=\"submit\" name=\"edit\" value=\"true\" class=\"btn btn-warning\"><img src=\"design\\bootstrap-icon\\pen.svg\" alt=\"Pen icon\"></button>";
                break;
            case 1:
                $c="success";
                break;
            case 2:
                $c="danger";
                break;
        }
        $badge=($value["Pen"]>0)?"warning":"light";
        $cmtBtn=($value["Comment"]!=0)?"":"disabled";
        ?><form method="post" action="?admin=art" class="border-bottom border-warning clearfix p-1">
            <span class="text-right font-weight-bold text-<?= $c ?> bg-<?=$c?> rounded px-2 mr-2"></span>
            <span class="font-weight-bold"><?= $value["Title"] ?></span> In : <?= $value["Cat"] ?> [By <?= $value["User"] ?> <span class="font-italic"><?= $value["Added"] ?></span>]
            <input type="hidden" name="editId" value="<?= $value["ArtId"] ?>">

            <button type="submit" name="del" value="true" class="btn btn-danger float-right" <?php
                if($value["Active"]==2){
                    echo "disabled";
                }
             ?>><img src="design/bootstrap-icon/trash.svg" alt="Trash Icon"></button>
             <a href="?admin=cmt&amp;art=<?= $value["ArtId"] ?>" class="btn btn-secondary float-right mx-2 <?= $cmtBtn ?>"><img src="design\bootstrap-icon\chat-text.svg" alt=""> <?= $value["Cmt"] ?>
                 <span class="badge badge-pill badge-<?= $badge ?>"><?= $value["Pen"] ?></span>
             </a>
            <?php
                if($value["Active"]!=2){ ?>
                    <button type="submit" name="edit" value="true" class="btn btn-warning float-right"><img src="design\bootstrap-icon\pen.svg" alt="Pen icon"></button>
                <?php }
            ?>
        </form><?php
    }
    $content=ob_get_clean();

    // Count total articles
    $cta=new GetDataFromDb();
    $ctaSql="SELECT COUNT(A.ID) CountArt FROM tb_art A JOIN tb_cat C ON A.CatID = C.ID";
    $cta->setSql($ctaSql);
    $rCta=$cta->getQuery($db);
    // Create pagination button
    ob_start();
    if($lim>=10){ ?>
        <a href="?admin=art&amp;lim=<?= $lim-10 ?>" class="btn btn-warning"><?= $ta["rec"] ?></a>
    <?php }
    if($lim+10<=$rCta["CountArt"]){ ?>
        <a href="?admin=art&amp;lim=<?= $lim+10 ?>" class="btn btn-warning"><?= $ta["old"] ?></a>
    <?php }
    $btnLim=ob_get_clean();

    // getCategory
    $gCat=new GetDataFromDb();
    $cSql="SELECT * FROM tb_cat WHERE IsActive != 2";
    $gCat->setSql($cSql);

    ob_start();
    foreach ($gCat->getMultiQuery($db) as $key => $value) {
        ?><option name="CatID" value="<?= $value["ID"] ?>" <?php
            if(@$e["CatID"] && @$e["CatID"]==$value["ID"]){
                echo "selected";
            }
        ?>><?= $value["Name"] ?></option><?php
    }
    $category=ob_get_clean();
