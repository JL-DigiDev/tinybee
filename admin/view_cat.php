<main class="container-fluid">
    <div class="row">
        <section class="col-md mx-5 my-2">
            <h1 class="alert alert-warning"><?= $tm["title"] ?></h1>
                <div class="container-fluid">
                    <div class="row">
                        <article class="col-md mx-0 my-1 px-2 border-right border-dark">
                            <h2><?= $tm["cat"] ?></h2>
                            <div class="alert alert-secondary text-center">
                                <span class="border border-success rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-success bg-success rounded px-2 mr-2"></span>
                                    <?= $tm["ex1"] ?>
                                </span>

                                <span class="border border-dark rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-dark bg-dark rounded px-2 mr-2"></span>
                                    <?= $tm["ex2"] ?>
                                </span>

                                <span class="border border-danger rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-danger bg-danger rounded px-2 mr-2"></span>
                                    <?= $tm["ex3"] ?>
                                </span>
                            </div>
                            <?= @$content ?>
                        </article>

                        <form class="col-md mx-0 my-1 px-2 pr-5 border-dark" action="?admin=cat" method="post">
                            <h2><?= $tm["add"] ?></h2>

                            <p class="mx-1">
                                <label for="newCat"><?= $tm["new"] ?></label>
                                <input type="text" name="newCat" value="" class="border border-warning rounded" />
                            </p>
                            <p class="mx-1 my-2 text-center">
                                <button type="submit" name="addCat" value="true" class="btn btn-warning text-center"><img src="design/bootstrap-icon/folder-plus.svg" alt="Folder Plus Icon"> <?= $tm["ok"] ?></button>
                            </p>
                        </form>
                    </div>
                </div>
        </section>
    </div>
</main>
