<?php
    $select=new GetDataFromDb();
    $sSql="SELECT C.ID, C.Name, C.Mail, C.Comment, C.IsActive, C.ArtID, C.Added , A.Name Article FROM tb_comment C JOIN tb_art A ON A.ID=C.ArtID";
    $seSql=" ORDER BY C.ID DESC";
    $url="?admin=cmt";

    switch ($_SESSION["lang"]) {
        case 'fr':
            $textCom=new FrenchComment();
            break;
        case 'en':
            $textCom=new EnglishComment();
            break;
        case 'nl':
            $textCom=new DutchComment();
            break;
    }

    $textCom->setComment();
    $tc=$textCom->getComment();

    if(@$_POST){
        $mSql="UPDATE tb_comment SET IsActive=:IsActive WHERE ID=:ID";
        $isActive=(@$_POST["valid"])?1:0;
        $cId=(@$_POST["valid"])?htmlentities($_POST["valid"]):htmlentities($_POST["delete"]);
        $mArr=array("IsActive"=>$isActive,"ID"=>$cId);

        $mStat=new GetDataFromDb();
        $mStat->setSql($mSql);
        $mStat->setData($mArr);
        $mStat->sendData($db);
    }

    if(@$_GET["art"]){
        $art=htmlentities($_GET["art"]);
        $sSql.=" WHERE C.ArtID=:ArtID".$seSql;
        $select->setSql($sSql);
        $select->setData(array("ArtID"=>$art));
        $comm=$select->getPreparedMultiQuery($db);
        $url.="&amp;art=$art";
    }else{
        $sSql.=$seSql;
        $select->setSql($sSql);
        $comm=$select->getMultiQuery($db);
    }

    $btnVal="<button type=\"submit\" name=\"valid\" value=\"";
    $btnValEnd="\" class=\"btn btn-success m-2\"><img src=\"design\\bootstrap-icon\\check2-circle.svg\" alt=\"Check Icon\"> ";
    $btnDel="<button type=\"submit\" name=\"delete\" value=\"";
    $btnDelEnd="\" class=\"btn btn-danger m-2\"><img src=\"design\\bootstrap-icon\\trash.svg\" alt=\"Trash Icon\"> ";
    $btnEnd="</button>";

    ob_start();
    foreach ($comm as $key => $value) {
        switch($value["IsActive"]){
            case 0:
                $stat="danger";
                $img="<img src=\"design\bootstrap-icon\x.svg\" alt=\"X (Deleted) Icon\">";
                $btn=$btnVal.$value["ID"].$btnValEnd.$tCom["sta-val"].$btnEnd;
                break;
            case 1:
                $stat="success";
                $img="<img src=\"design\bootstrap-icon\check-all.svg\" alt=\"CheckAll Icon\">";
                $btn=$btnDel.$value["ID"].$btnDelEnd.$tCom["mod-del"].$btnEnd;
                break;
            case 2:
                $stat="dark";
                $img="<img src=\"design\bootstrap-icon\hourglass.svg\" alt=\"Hourglass Icon\">";
                $btn=$btnVal.$value["ID"].$btnValEnd.$tCom["sta-val"].$btnEnd.$btnDel.$value["ID"].$btnDelEnd.$tCom["mod-del"].$btnEnd;
                break;
        }
        $sp=$value["Name"]." ".$tCom["com-on"]." ".$value["Article"];
        ?>
        <form class="border border-warning rounded mb-2 justify-content-center" action="<?= $url ?>" method="post">
            <div class="alert alert-<?= $stat ?> border-bottom border-<?= $stat ?>">
                <span class="float-left mx-2"><?= $img ?> <?= $sp ?></span>
                <span class="float-right"><?= $value["Added"] ?></span>
                <div class="clearfix">

                </div>
            </div>

            <div class="m-2">
                <?= nl2br($value["Comment"]) ?>
            </div>

            <section class="form-row justify-content-center">
                 <?= $btn ?>
            </section>
        </form>
    <?php }
    $cData=ob_get_clean();
