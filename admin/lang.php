<?php
    class FrenchNav{
        public $nav=array();

        function setNav(){
            $this->nav["home"]="Accueil";
            $this->nav["usr"]="Utilisateurs";
            $this->nav["art"]="Articles";
            $this->nav["cat"]="Catégories";
            $this->nav["set"]="Configuration";
            $this->nav["quit"]="Déconnexion";
            $this->nav["lang"]="Langue";
            $this->nav["cmt"]="Commentaires";
        }
        function getNav(){
            return $this->nav;
        }
    }

    class EnglishNav extends FrenchNav{
        function setNav(){
            $this->nav["home"]="Home";
            $this->nav["usr"]="Users";
            $this->nav["art"]="Articles";
            $this->nav["cat"]="Category";
            $this->nav["set"]="Settings";
            $this->nav["quit"]="Disconnect";
            $this->nav["lang"]="Langage";
            $this->nav["cmt"]="Comments";
        }
    }

    class DutchNav extends FrenchNav{
        function setNav(){
            $this->nav["home"]="Ontvangst";
            $this->nav["usr"]="Gerbruikers";
            $this->nav["art"]="lidwoord";
            $this->nav["cat"]="Categorie";
            $this->nav["set"]="Instellingen";
            $this->nav["quit"]="Verbinding verbreken";
            $this->nav["lang"]="Talen";
            $this->nav["cmt"]="Opmerkingen";
        }
    }

    class FrenchConn{
        public $conn=array();

        function setConn(){
            $this->conn["conn"]="Connexion";
            $this->conn["mail"]="Adresse email";
            $this->conn["pass"]="Mot de passe";
            $this->conn["f-m"]="Adresse email inconnue...";
            $this->conn["f-p"]="Mot de passe incorrect...";
        }
        function getConn(){
            return $this->conn;
        }
    }

    class EnglishConn extends FrenchConn{
        function setConn(){
            $this->conn["conn"]="Connection";
            $this->conn["mail"]="email";
            $this->conn["pass"]="Password";
            $this->conn["f-m"]="Unknow email...";
            $this->conn["f-p"]="Bad password...";
        }
    }

    class DutchConn extends FrenchConn{
        function setConn(){
            $this->conn["conn"]="Verbionding";
            $this->conn["mail"]="e-mail";
            $this->conn["pass"]="Wachtwoord";
            $this->conn["f-m"]="Onbekende email...";
            $this->conn["f-p"]="Slecht wachtwoord...";
        }
    }

    class FrenchAdmin{
        public $adm=array();

        function setAdmin(){
            $this->adm["title"]="Paramètres de";
            $this->adm["blog"]="Nom du blog";
            $this->adm["theme"]="Thème";
            $this->adm["mod"]="Modifier";
            $this->adm["logo"]="Logo / Bannière";
            $this->adm["ico"]="Icône";
            $this->adm["cmt"]="Commentaires";
        }
        function getAdmin(){
            return $this->adm;
        }
    }

    class EnglishAdmin extends FrenchAdmin{
        function setAdmin(){
            $this->adm["title"]="Settings of";
            $this->adm["blog"]="Blog Name";
            $this->adm["theme"]="Theme";
            $this->adm["mod"]="Modify";
            $this->adm["logo"]="Logo / Banner";
            $this->adm["ico"]="Icon";
            $this->adm["cmt"]="Comments";
        }
    }

    class DutchAdmin extends FrenchAdmin{
        function setAdmin(){
            $this->adm["title"]="Instellingen van";
            $this->adm["blog"]="Blog Naam";
            $this->adm["theme"]="Thema";
            $this->adm["mod"]="Aanpassen";
            $this->adm["logo"]="Logo / Banner";
            $this->adm["ico"]="Icoon";
            $this->adm["cmt"]="Opmerkingen";
        }
    }

    class FrenchCommon{
        public $com=array();

        function setCommon(){
            $this->com["com-on"]="dans";
            $this->com["sta-ena"]="Actif";
            $this->com["sta-dis"]="Inactif";
            $this->com["sta-pen"]="Actif avec Modération";
            $this->com["sta-del"]="Supprimé";
            $this->com["sta-dra"]="Brouillon";
            $this->com["sta-val"]="Valider";
            $this->com["mod-del"]="Supprimer";
        }
        function getCommon(){
            return $this->com;
        }
    }

    class EnglishCommon extends FrenchCommon{
        function setCommon(){
            $this->com["com-on"]="in";
            $this->com["sta-ena"]="Enabled";
            $this->com["sta-dis"]="Disabled";
            $this->com["sta-pen"]="Enabled with Pending";
            $this->com["sta-del"]="Deleted";
            $this->com["sta-dra"]="Draft";
            $this->com["sta-val"]="Valid";
            $this->com["mod-del"]="Delete";
        }
    }

    class DutchCommon extends FrenchCommon{
        function setCommon(){
            $this->com["com-on"]="in";
            $this->com["sta-ena"]="Ingeschakeld";
            $this->com["sta-dis"]="Uitgeschakeld";
            $this->com["sta-pen"]="Ingeschakeld met in behandeling";
            $this->com["sta-del"]="Verwijderd";
            $this->com["sta-dra"]="Klad";
            $this->com["sta-val"]="Geldig";
            $this->com["mod-del"]="Verwijderd";
        }
    }

    class FrenchCat{
        public $cat=array();

        function setCat(){
            $this->cat["title"]="Gestion des catégories";
            $this->cat["add"]="Ajouter une catégorie";
            $this->cat["del"]="Supprimer";
            $this->cat["cat"]="Catégories";
            $this->cat["new"]="Nom de la catégorie";
            $this->cat["ok"]="Ajouter";
            $this->cat["ex1"]="Actif";
            $this->cat["ex2"]="Inactif";
            $this->cat["ex3"]="Supprimé";
            $this->cat["mrg"]="Fusionner avec";
        }
        function getCat(){
            return $this->cat;
        }
    }

    class EnglishCat extends FrenchCat{
        function setCat(){
            $this->cat["title"]="Category management";
            $this->cat["add"]="Add a category";
            $this->cat["del"]="Delete";
            $this->cat["cat"]="Category";
            $this->cat["new"]="Category name";
            $this->cat["ok"]="Add";
            $this->cat["ex1"]="Enabled";
            $this->cat["ex2"]="Disabled";
            $this->cat["ex3"]="Deleted";
            $this->cat["mrg"]="Merge with";
        }
    }

    class DutchCat extends FrenchCat{
        function setCat(){
            $this->cat["title"]="Categorie beheer";
            $this->cat["add"]="Voeg een categorie toe";
            $this->cat["del"]="Verwijderen";
            $this->cat["cat"]="Categorie";
            $this->cat["new"]="Categorie naam";
            $this->cat["ok"]="Toevoegen";
            $this->cat["ex1"]="Ingeschakeld";
            $this->cat["ex2"]="Uitgeschakeld";
            $this->cat["ex3"]="Verwijderd";
            $this->cat["mrg"]="Fuseren met";
        }
    }

    class FrenchArt{
        public $art=array();

        function setArt(){
            $this->art["title"]="Gestion des articles";
            $this->art["aTit"]="Titre";
            $this->art["new"]="Nouvel article";
            $this->art["cat"]="Catégorie";
            $this->art["draft"]="Brouillon";
            $this->art["post"]="Publier";
            $this->art["lst"]="Liste des articles";
            $this->art["online"]="En ligne";
            $this->art["del"]="Supprimer";
            $this->art["era"]="Effacé";
            $this->art["cmt"]="Désactiver les commentaires";
            $this->art["rec"]="Plus récents";
            $this->art["old"]="Plus anciens";
        }

        function getArt(){
            return $this->art;
        }
    }

    class EnglishArt extends FrenchArt{
        function setArt(){
            $this->art["title"]="Post management";
            $this->art["aTit"]="Title";
            $this->art["new"]="New post";
            $this->art["cat"]="Category";
            $this->art["draft"]="Draft";
            $this->art["post"]="Publish";
            $this->art["lst"]="Post list";
            $this->art["online"]="Online";
            $this->art["del"]="Delete";
            $this->art["era"]="Erased";
            $this->art["cmt"]="Comment disable";
            $this->art["rec"]="Recent";
            $this->art["old"]="Older";
        }
    }

    class DutchArt extends FrenchArt{
        function setArt(){
            $this->art["title"]="Postbeheer";
            $this->art["aTit"]="Titel";
            $this->art["new"]="Nieuw bericht";
            $this->art["cat"]="Categorie";
            $this->art["draft"]="Draft";
            $this->art["post"]="Publiceren";
            $this->art["lst"]="Postlijst";
            $this->art["online"]="Online";
            $this->art["del"]="Verwijderen";
            $this->art["era"]="Gewist";
            $this->art["cmt"]="Reactie uitschakelen";
            $this->art["rec"]="Recent";
            $this->art["old"]="Ouder";
        }
    }

    class FrenchUser{
        public $usr=array();

        function setUsr(){
            $this->usr["title"]="Gestion des utilisateurs";
            $this->usr["name"]="Nom";
            $this->usr["mail"]="Email";
            $this->usr["pass"]="Mot de passe";
            $this->usr["acc"]="Compte";
            $this->usr["you"]="Vous";
            $this->usr["new"]="Nouvel utilisateur";
            $this->usr["reg"]="Utilisateurs enregistrés";
            $this->usr["modp"]="Modifier votre mot de passe";
            $this->usr["curp"]="Mot de passe actuel";
            $this->usr["newp"]="Nouveau mot de passe";
            $this->usr["conp"]="Confirmez le nouveau mot de passe";
            $this->usr["mody"]="Modifier vos informations";
            $this->usr["mod"]="Modifier";
            $this->usr["add"]="Ajouter";
            $this->usr["crep"]="Créer mot de passe";
            $this->usr["genp"]="Générer Mot de passe";
            $this->usr["del"]="Supprimer";
        }

        function getUsr(){
            return $this->usr;
        }
    }

    class EnglishUser extends FrenchUser{
        function setUsr(){
            $this->usr["title"]="Users management";
            $this->usr["name"]="Name";
            $this->usr["mail"]="Email";
            $this->usr["pass"]="Password";
            $this->usr["acc"]="Account";
            $this->usr["you"]="You";
            $this->usr["new"]="New user";
            $this->usr["reg"]="Registered users";
            $this->usr["modp"]="Modify your password";
            $this->usr["curp"]="Current password";
            $this->usr["newp"]="New password";
            $this->usr["conp"]="Confirm new password";
            $this->usr["mody"]="Modify your informations";
            $this->usr["mod"]="Modify";
            $this->usr["add"]="Add";
            $this->usr["crep"]="Create password";
            $this->usr["genp"]="Generate password";
            $this->usr["del"]="Remove";
        }
    }

    class DutchUser extends FrenchUser{
        function setUsr(){
            $this->usr["title"]="Gebruikersbeheer";
            $this->usr["name"]="Naam";
            $this->usr["mail"]="E-mail";
            $this->usr["pass"]="Wachtwoord";
            $this->usr["acc"]="Account";
            $this->usr["you"]="Jij";
            $this->usr["new"]="Nieuwe gebruiker";
            $this->usr["reg"]="Geregistreerde gebruikers";
            $this->usr["modp"]="Wijzig uw wachtwoord";
            $this->usr["curp"]="Huidig wachtwoord";
            $this->usr["newp"]="Nieuw wachtwoord";
            $this->usr["conp"]="Bevestig nieuw wachtwoord";
            $this->usr["mody"]="Wijzig uw gegevens";
            $this->usr["mod"]="Wijzigen";
            $this->usr["add"]="Toevoegen";
            $this->usr["crep"]="Maak wachtwoord aan";
            $this->usr["genp"]="Genereer wachtwoord";
            $this->usr["del"]="Verwijderen";
        }
    }

    class FrenchComment{
        public $comment=array();

        function setComment(){
            $this->comment["title"]="Gestion des Commentaires";
        }
        function getComment(){
            return $this->comment;
        }
    }

    class EnglishComment extends FrenchComment{
        function setComment(){
            $this->comment["title"]="Comments Management";
        }
    }

    class DutchComment extends FrenchComment{
        function setComment(){
            $this->comment["title"]="Beheer van opmerkingen";
        }
    }
