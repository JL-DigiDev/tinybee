<main class="container-fluid">
    <div class="row">
        <section class="col-md mx-5 my-2">
            <h1 class="alert alert-warning"><?= $tc["title"] ?></h1>
                <div class="container-fluid">
                    <div class="row">
                        <article class="col-md mx-0 my-1 px-2">
                            <?= $cData ?>
                        </article>
                    </div>
                </div>
            </h1>
        </section>
    </div>
</main>
