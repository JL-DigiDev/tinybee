<?php
    function userConn($db,$m,$p,$c){
        $r_uc=$db->prepare("SELECT * FROM tb_user WHERE Mail = :m AND IsActive = 1");
        $r_uc->execute(array("m"=>$m));
        $d_uc=$r_uc->fetch();
        $r_uc->closeCursor();

        if($d_uc){
            if(password_verify($p,$d_uc["Password"])){
                $_SESSION["UserID"]=$d_uc["ID"];
                $_SESSION["Name"]=$d_uc["Name"];
                $_SESSION["Mail"]=$d_uc["Mail"];
                $_SESSION["Register"]=$d_uc["Register"];
                $_SESSION["connected"]=true;
                session_encode();

                $s=$_SERVER["HTTP_HOST"];
                $u=$_SERVER["PHP_SELF"];
                header("Location: http://$s$u?admin=main");
                exit;
            }else{
                return array("cls"=>"alert alert-danger","message"=>$c["f-p"]);
            }
        }else{
            return array("cls"=>"alert alert-danger","message"=>$c["f-m"]);
        }
    }

    switch($_SESSION["lang"]){
        case "fr":
            $cl=new FrenchConn();
            break;
        case "en":
            $cl=new EnglishConn();
            break;
        case "nl":
            $cl=new DutchConn();
            break;
    }

    $cl->setConn();
    $c=$cl->getConn();

    $cls="btn-secondary";
    $uri="admin=c";

    if(@$_POST){
        $show=userConn($db,htmlentities($_POST["mail"]),htmlentities($_POST["password"]),$c);
    }
