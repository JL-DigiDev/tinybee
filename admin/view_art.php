<main class="container-fluid">
    <div class="row">
        <section class="col-md m-5">
            <h1 class="alert alert-warning"><?= $ta["title"] ?></h1>
                <div class="container-fluid">
                    <div class="row">
                        <article class="col-md mx-0 my-1 px-2 border-right border-dark">
                            <h2><?= $ta["lst"] ?></h2>
                            <div class="alert alert-secondary text-center">
                                <span class="border border-success rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-success bg-success rounded px-2 mr-2"></span>
                                    <?= $ta["online"] ?>
                                </span>

                                <span class="border border-dark rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-dark bg-dark rounded px-2 mr-2"></span>
                                    <?= $ta["draft"] ?>
                                </span>

                                <span class="border border-danger rounded m-5 p-2">
                                    <span class="text-right font-weight-bold text-danger bg-danger rounded px-2 mr-2"></span>
                                    <?= $ta["era"] ?>
                                </span>
                            </div>
                            <?= @$content ?>

                            <div class="text-center p-1">
                                <?= $btnLim ?>
                            </div>
                        </article>

                        <form class="col-md mx-0 my-1 px-2 pr-5 border-dark" action="?admin=art" method="post">
                            <h2><?= $ta["new"] ?></h2>

                            <p class="mx-1">
                                <?= @$eHidden ?>
                                <label for="Name"><?= $ta["aTit"] ?></label> : <input type="text" name="Name" value="<?= @$e["Name"] ?>" class="border border-warning rounded" required />
                                </p>

                            <p class="mx-1">
                                <script type="text/javascript" src="scripts/text-format.js"></script>

                                <input type="hidden" name="lang" value="<?= $_SESSION["lang"] ?>" id="lang" />

                                <!-- Start Buttons Editor -->
                                <button type="button" name="btn-bold" id="btn-b" class="btn btn-warning" onclick="Format('b')">
                                    <img src="<?= FICO ?>type-bold.svg" alt="Bold Icon">
                                </button>

                                <button type="button" name="btn-italic" id="btn-i" class="btn btn-warning" onclick="Format('i')">
                                    <img src="<?= FICO ?>type-italic.svg" alt="Italic Icon">
                                </button>

                                <button type="button" name="btn-underline" id="btn-u" class="btn btn-warning" onclick="Format('u')">
                                    <img src="<?= FICO ?>type-underline.svg" alt="Underline Icon">
                                </button>

                                <button type="button" name="btn-strikethrough" id="btn-s" class="btn btn-warning" onclick="Format('s')">
                                    <img src="<?= FICO ?>type-strikethrough.svg" alt="Strikethrough Icon">
                                </button>

                                <button type="button" name="btn-title" class="btn btn-warning" onclick="Title()">
                                    <img src="<?= FICO ?>type-h1.svg" alt="Title H1 Icon">
                                </button>

                                <button type="button" name="btn-list" class="btn btn-warning" onclick="List('ul')">
                                    <img src="<?= FICO ?>list-ul.svg" alt="List Icon">
                                </button>

                                <button type="button" name="btn-list-ol" class="btn btn-warning" onclick="List('ol')">
                                    <img src="<?= FICO ?>list-ol.svg" alt="List OL Icon">
                                </button>

                                <button type="button" name="btn-img" class="btn btn-warning" onclick="Picture()">
                                    <img src="<?= FICO ?>image.svg" alt="Image Icon">
                                </button>

                                <button type="button" name="btn-link" class="btn btn-warning" onclick="Link()">
                                    <img src="<?= FICO ?>link.svg" alt="Link Icon">
                                </button>
                                <!-- End Buttons Editor -->
                                <br>
                                <textarea name="Content" rows="8" cols="80" class="border border-warning rounded text-left" id="postContent" contenteditable="true" required><?= @$e["Content"] ?></textarea><br>
                                <?= @$eHidden ?>
                            </p>

                            <p class="mx-1">
                                <label for="CatID"><?= $ta["cat"] ?></label> : <select class="border border-warning rounded" name="CatID">
                                    <?= @$category ?>
                                </select>
                            </p>

                            <p class="mx-1">
                                <label for="IsActive"><?= $ta["draft"] ?></label> : <input type="checkbox" name="IsActive" value="0" <?= @$isDraft ?> >
                            </p>

                            <p class="mx-1">
                                <label for="Comment"><?= $ta["cmt"] ?></label> : <input type="checkbox" name="Comment" value="0">
                            </p>

                            <p class="mx-1 my-2 text-center">
                                <button type="submit" name="addArt" value="true" class="btn btn-warning text-center"><img src="design/bootstrap-icon/journal-check.svg" alt="Journal Check Icon"> <?= $ta["post"] ?></button>
                            </p>
                        </form>
                    </div>
                </div>
        </section>
    </div>
</main>
