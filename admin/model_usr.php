<?php
    switch ($_SESSION["lang"]) {
        case 'fr':
            $textUser=new FrenchUser();
            break;
        case 'en':
            $textUser=new EnglishUser();
            break;
        case 'nl':
            $textUser=new DutchUser();
            break;
    }

    $id=htmlentities($_SESSION["UserID"]);

    if(@$_POST["userData"]){
        $userName=htmlentities($_POST["uName"]);
        $userMail=htmlentities($_POST["uMail"]);

        $uuSql="UPDATE tb_user SET Name = :Name, Mail = :Mail WHERE ID = :ID";
        $uuArr=array("Name"=>$userName,"Mail"=>$userMail,"ID"=>$id);

        $userUpd=new GetDataFromDb();
        $userUpd->setSql($uuSql);
        $userUpd->setData($uuArr);
        $userUpd->sendData($db);

        $_SESSION["Name"]=$userName;
        $_SESSION["Mail"]=$userMail;
    }else if(@$_POST["userPassword"]){
        $oldPassword=htmlentities($_POST["ucPassword"]);
        $newPassword=htmlentities($_POST["unPassword"]);
        $vrfPassword=htmlentities($_POST["uncPassword"]);

        $upSql="SELECT Password FROM tb_user WHERE ID = :ID";
        $tryPassword=new GetDataFromDb();
        $tryPassword->setSql($upSql);
        $tryPassword->setData(array("ID"=>$id));
        $curPassword=$tryPassword->getPreparedQuery($db);

        if(password_verify($oldPassword,$curPassword["Password"])){
            if($newPassword==$vrfPassword){
                $npSql="UPDATE tb_user SET Password = :Password WHERE ID = :ID";
                $npArr=array("Password"=>password_hash($newPassword,PASSWORD_DEFAULT),"ID"=>$id);
                $sendPassword=new GetDataFromDb();
                $sendPassword->setSql($npSql);
                $sendPassword->setData($npArr);
                $sendPassword->sendData($db);
            }
        }
    }else if(@$_POST["newUser"]){
        $arrNewUser=array();
        foreach ($_POST as $key => $value) {
            $arrNewUser[$key]=htmlentities($value);
        }
        $uName=$arrNewUser["nuName"];
        $uMail=$arrNewUser["nuMail"];
        if($arrNewUser["nuPassword"]=="created"){
            if($arrNewUser["nucPassword"]==$arrNewUser["nuvPassword"]){
                $uPassword=$arrNewUser["nucPassword"];
            }
        }else if($arrNewUser["nuPassword"]=="generated"){
            $uPassword=$arrNewUser["nugPassword"];
        }
        $nuSql="INSERT INTO tb_user(Name,Mail,Password) VALUES(:Name,:Mail,:Password)";
        $newUser=new GetDataFromDb();
        $newUser->setSql($nuSql);
        $newUser->setData(array("Name"=>$uName,"Mail"=>$uMail,"Password"=>password_hash($uPassword,PASSWORD_DEFAULT)));
        $newUser->sendData($db);
    }else if(@$_POST["deleteUser"]){
        $delUser=new GetDataFromDb();
        $duSql="UPDATE tb_user SET IsActive = 0 WHERE ID = :ID";
        $delUser->setSql($duSql);
        $delUser->setData(array("ID"=>htmlentities($_POST["deleteUser"])));
        $delUser->sendData($db);
    }

    $name=htmlentities($_SESSION["Name"]);
    $mail=htmlentities($_SESSION["Mail"]);

    $textUser->setUsr();
    $tu=$textUser->getUsr();

    $generatedPassword = new GenPassword();
    $generatedPassword->createPassword();
    $gp=$generatedPassword->getPassword();

    $connectedUser=new GetDataFromDb();
    // $connectedUser=

    $ulSql="SELECT ID, Name, IsActive FROM tb_user";
    $ul=new GetDataFromDb();
    $ul->setSql($ulSql);
    $rUserList=$ul->getMultiQuery($db);

    ob_start();
    foreach ($rUserList as $key => $value) {
        $cs=($value["IsActive"]==1)?"success":"dark";
        $ba=($value["IsActive"]==1)?"":"disabled"; ?>
        <div class="border-bottom border-warning clearfix py-1 m-1 text-left">
            <span class="text-right font-weight-bold text-<?= $cs ?> bg-<?= $cs ?> rounded px-2 mr-2"></span>
            <?= $value["Name"] ?>
            <button type="submit" name="deleteUser" value="<?= $value["ID"] ?>" class="btn btn-danger float-right <?= $ba ?>">
                <img src="design/bootstrap-icon/person-dash.svg" alt="Person Dash icon"> <?= $tu["del"] ?>
            </button>
        </div>
    <?php }
    $userList=ob_get_clean();
