<main class="container-fluid">
    <div class="row justify-content-center">
        <h1 class="alert alert-warning col-md mx-5 my-2"><?= $tu["title"] ?></h1>

        <?= @$alert ?>
    </div>

    <div class="row text-center">
        <div class="border border-warning rounded col-md mx-5 my-2 p-0">
            <h2 class="bg-warning p-1"><?= $tu["you"] ?></h2>
            <form class="border-bottom border-warning" action="?admin=usr" method="post">
                <h3><?= $tu["mody"] ?></h3>
                <label for="uName"><?= $tu["name"] ?></label><br>
                    <input type="text" name="uName" value="<?= $name ?>"><br>
                <label for="uMail"><?= $tu["mail"] ?></label><br>
                    <input type="email" name="uMail" value="<?= $mail ?>"><br>
                <button type="submit" name="userData" value="true" class="btn btn-warning m-2"><?= $tu["mod"] ?></button>
            </form>

            <form class="border-top border-warning" action="?admin=usr" method="post">
                <h3><?= $tu["modp"] ?></h3>
                <label for="ucPassword"><?= $tu["curp"] ?></label><br>
                    <input type="password" name="ucPassword" value="" required><br>
                <label for="unPassword"><?= $tu["newp"] ?></label><br>
                    <input type="password" name="unPassword" value="" required><br>
                <label for="uncPassword"><?= $tu["conp"] ?></label><br>
                    <input type="password" name="uncPassword" value="" required><br>
                <button type="submit" name="userPassword" value="true" class="btn btn-warning m-2"><?= $tu["mod"] ?></button>
            </form>
        </div>



        <form class="border border-warning rounded col-md mx-5 my-2 p-0" action="?admin=usr" method="post">
            <h2 class="bg-warning p-1"><?= $tu["new"] ?></h2>
            <label for="nuName"><?= $tu["name"] ?></label><br>
                <input type="text" name="nuName" value="" required><br>
            <label for="nuMail"><?= $tu["mail"] ?></label><br>
                <input type="email" name="nuMail" value="" required><br>

            <div class="p-2 mt-2 border-top border-bottom border-warning">
                <h3><input type="radio" name="nuPassword" value="created" class="m-2"><?= $tu["crep"] ?></h3>
                <label for="nucPassword"><?= $tu["newp"] ?></label><br>
                    <input type="password" name="nucPassword" value=""><br>
                <label for="nuvPassword"><?= $tu["conp"] ?></label><br>
                    <input type="password" name="nuvPassword" value=""><br>
            </div>

            <div class="p-2 border-top border-bottom border-warning">
                <h3><input type="radio" name="nuPassword" value="generated" class="m-2" checked><?= $tu["genp"] ?></h3>
                <div class="alert alert-warning">
                    <input type="hidden" name="nugPassword" value="<?= $gp ?>">
                    <?= $gp ?>
                </div>
            </div>

            <div class="m-2">
                <button type="submit" name="newUser" value="true" class="btn btn-warning m-2">
                    <img src="design/bootstrap-icon/person-plus.svg" alt="Add Person Icon"> <?= $tu["add"] ?>
                </button>
            </div>
        </form>

        <form class="border border-warning rounded col-md mx-5 my-2 p-0" action="?admin=usr" method="post">
            <h2 class="bg-warning p-1"><?= $tu["reg"] ?></h2>
            <?= $userList ?>
        </form>
    </div>
</main>
