<main class="container-fluid">
    <div class="row">
        <form class="col-md mx-5 my-2 px-0 py-2 border border-warning rounded bg-warning text-center" action="?admin=connect" method="post">
            <h1 class="p-1"><img src="design/TinyBee-Icon.svg" alt="TinyBee Icon" height="64px" >TinyBee <?= $c["conn"] ?></h1>
            <section class="my-2 mx-5 text-left">
                <?php require("admin/nav-lang.php"); ?>
            </section>
            <div class="bg-white py-2 rounded">
                <article class="<?= @$show["cls"] ?> mx-5 my-2">
                    <?= @$show["message"] ?>
                </article>
                <label for="mail" class="col-md"><?= $c["mail"] ?></label>
                    <input type="email" name="mail" value="" class="col-md-6 rounded border border-dark " required />
                <label for="password" class="col-md"><?= $c["pass"] ?></label>
                    <input type="password" name="password" value="" class="col-md-6 rounded border border-dark " required />
            </div>
            <button type="submit" name="submit" class="col-md-2 btn btn-success m-2"><img src="design/bootstrap-icon/key.svg" alt="Key Icon"> <?= $c["conn"] ?></button>
        </form>
    </div>
</main>
