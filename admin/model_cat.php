<?php
    $gSql="SELECT * FROM tb_cat";

    $mainData=new GetDataFromDb();
    $mainData->setSql($gSql);
    $blogData=array();

    switch ($_SESSION["lang"]) {
        case 'fr':
            $textMain=new FrenchCat();
            break;
        case 'en':
            $textMain=new EnglishCat();
            break;
        case 'nl':
            $textMain=new DutchCat();
            break;
    }

    if(@$_POST){
        if(@$_POST["addCat"]){
            $sSql="INSERT INTO tb_cat(Name) VALUES(:Name)";
            $rArr=array();

            $rArr["Name"]=htmlentities($_POST["newCat"]);

            $sendData=new GetDataFromDb();
            $sendData->setSql($sSql);
            $sendData->setData($rArr);
            $sendData->sendData($db);
        }

        if(@$_POST["c-s"]){
            $chSql="UPDATE tb_cat SET IsActive = IF(IsActive=0,1,0) WHERE ID = :id";
            $chArr=array("id"=>htmlentities($_POST["CatId"]));

            $ch=new GetDataFromDb();
            $ch->setSql($chSql);
            $ch->setData($chArr);
            $ch->sendData($db);
        }

        if(@$_POST["delAll"]){
            $daSql="UPDATE tb_art SET IsActive = 2 WHERE CatId = :ID";
            $daArr=array("ID"=>htmlentities($_POST["CatId"]));
            $dcSql="UPDATE tb_cat SET IsActive = 2 WHERE ID = :ID";

            $da=new GetDataFromDb();
            $da->setSql($daSql);
            $da->setData($daArr);
            $da->sendData($db);
            $del=new GetDataFromDb();
            $del->setSql($dcSql);
            $del->setData($daArr);
            $del->sendData($db);
        }
        if(@$_POST["merge"]){
            $mrgSql="UPDATE tb_art SET CatId = :NewCat WHERE CatId = :OldCat";
            $mrgArr=array("NewCat"=>htmlentities($_POST["mergeWith"]),"OldCat"=>htmlentities($_POST["CatId"]));
            $delSql="UPDATE tb_cat SET IsActive = 2 WHERE ID = :ID";
            $delArr=array("ID"=>htmlentities($_POST["CatId"]));

            $mrg=new GetDataFromDb();
            $mrg->setSql($mrgSql);
            $mrg->setData($mrgArr);
            $mrg->sendData($db);
            $del=new GetDataFromDb();
            $del->setSql($delSql);
            $del->setData($delArr);
            $del->sendData($db);
        }
    }

    $textMain->setCat();
    $tm=$textMain->getCat();

    $md=$mainData->getMultiQuery($db);

    ob_start();
    foreach ($md as $key => $value) {
        switch($value["IsActive"]){
            case 0:
                $c="dark";
                $b="<img src=\"design\\bootstrap-icon\\eye.svg\" alt=\"Icon Eye\">";
                break;
            case 1:
                $c="success";
                $b="<img src=\"design\\bootstrap-icon\\eye-slash.svg\" alt=\"Icon Eye Slash\">";
                break;
            case 2:
                $c="danger";
                $d="disabled";
                break;
        }
        ?>
            <form class="border-bottom border-warning clearfix py-1" method="post" action="?admin=cat">
                <span class="text-right font-weight-bold text-<?= $c ?> bg-<?=$c?> rounded px-2 mr-2"></span>
                <?php echo $value["Name"];
                if(@$b){ ?>
                    <input type="hidden" name="CatId" value="<?= $value["ID"] ?>" />
                    <button type="submit" name="c-s" value="true" class="btn btn-warning float-right" <?= @$d ?>><?= $b ?></button>

                    <button type="submit" name="delAll" value="true" class="btn btn-warning mx-2 float-right" <?= @$d ?>>
                        <img src="design/bootstrap-icon/trash.svg" alt="Trash Icon" class="mx-2" >
                    </button>

                    <div class="btn-group float-right mx-2">
                        <button type="button" name="button" class="btn btn-warning" <?= @$d ?>>
                            <select class="border rounded" name="mergeWith"><?php
                            foreach ($md as $k => $v) {
                                if($k!=$key && $v["IsActive"]!=2){ ?>
                                <option value="<?= $v["ID"] ?>"><?= $v["Name"] ?></option> <?php }
                            } ?>
                            </select>
                        </button>
                        <button type="submit" name="merge" value="true" class="btn btn-warning" <?= @$d ?>>
                            <img src="design/bootstrap-icon/fusion.svg" alt="Fusion Icon" class="mx-2" >
                        </button>
                    </div>
                <?php } ?>
            </form>
        <?php
    }
    $content=ob_get_clean();
