<?php
    require("lang.php");

    lang();

    switch($_SESSION["lang"]){
        case "fr":
            $aNav=new FrenchNav();
            $com=new FrenchCommon();
            break;
        case "en":
            $aNav=new EnglishNav();
            $com=new EnglishCommon();
            break;
        case "nl":
            $aNav=new DutchNav();
            $com=new DutchCommon();
            break;
    }
    $aNav->setNav();
    $n=$aNav->getNav();

    $com->setCommon();
    $tCom=$com->getCommon();

    require_once("header.php");
    if(@$_SESSION["connected"]){
        require_once("nav.php");
        $g=htmlentities(@$_GET["admin"]);
        $a=array("art","cat","usr","set","cmt");
        if(in_array($g,$a)){
            require_once("model_$g.php");
            require_once("view_$g.php");
        }else if($g=="quit"){
            $tLang=$_SESSION["lang"];
            session_destroy();
            session_start();
            $_SESSION["lang"]=$tLang;

            $s=$_SERVER["HTTP_HOST"];
            $u=$_SERVER["PHP_SELF"];
            header("Location: http://$s$u?admin=c");
            exit;
        }else{
            require_once("model_main.php");
            require_once("view_main.php");
        }
    }else{
        require_once("model_connect.php");
        require_once("view_connect.php");
    }
    require_once("footer.php");
