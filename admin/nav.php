    <nav class="navbar navbar-expand-sm bg-warning">
        <span class="navbar-brand mb-0 h1"><a href="?"><img src="design/TinyBee-Icon.svg" alt="TinyBee Icon" height="64px" ></a></span>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headNav" aria-controls="headNav" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon">
                <img src="design/bootstrap-icon/grid.svg" alt="">
            </span>
        </button>

        <div class="collapse navbar-collapse offset-sm-1 col-sm-10 h-5 bg-warning" id="headNav">
            <ul class="navbar-nav mr-auto bg-warning">
                <li class="nav-item mi"><a href="index.php?admin=home" class="nav-link text-white font-weight-bold"><?= $n["home"] ?></a></li>
                <li class="nav-item mi"><a href="index.php?admin=art" class="nav-link text-white font-weight-bold"><?= $n["art"] ?></a></li>
                <li class="nav-item mi"><a href="index.php?admin=cmt" class="nav-link text-white font-weight-bold"><?= $n["cmt"] ?></a></li>
                <li class="nav-item mi"><a href="index.php?admin=cat" class="nav-link text-white font-weight-bold"><?= $n["cat"] ?></a></li>
                <li class="nav-item mi"><a href="index.php?admin=usr" class="nav-link text-white font-weight-bold"><?= $n["usr"] ?></a></li>
                <li class="nav-item mi"><a href="index.php?admin=quit" class="nav-link text-white font-weight-bold"><?= $n["quit"] ?></a></li>
            </ul>
        </div>
    </nav>
