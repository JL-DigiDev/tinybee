<main class="container-fluid">
    <div class="row justify-content-center">
        <h1 class="alert alert-warning col-md mx-5 my-2"><?= $title ?></h1>
    </div>

    <div class="row">
        <section class="border border-warning rounded col-md mx-5 my-2 p-2">
            <?php require("admin/nav-lang.php"); ?>
        </section>
    </div>

    <div class="row text-center">
        <form class="<?= FC ?>" action="<?= FU ?>" method="post">
            <h2 class="<?= FCH ?>"><?= $tm["blog"] ?></h2>
            <input type="text" name="blogName" value="<?= $blogData["blogName"] ?>" class="<?= FCI ?>"><br>
            <button type="submit" name="data" value="true" class="<?= FCB ?>"><?= $tm["mod"] ?></button>
        </form>

        <form class="<?= FC ?>" action="<?= FU ?>" method="post">
            <h2 class="<?= FCH ?>"><?= $tm["theme"] ?></h2>
            <select class="<?= FCI ?>" name="blogTheme">
                <?= $theme ?>
            </select>
            <button type="submit" name="data" value="true" class="<?= FCB ?>"><?= $tm["mod"] ?></button>
        </form>

        <form class="<?= FC ?>" action="<?= FU ?>" method="post">
            <h2 class="<?= FCH ?>"><?= $tm["cmt"] ?></h2>
            <select class="<?= FCI ?>" name="blogComment">
                <?= $cmtOpt ?>
            </select>
            <button type="submit" name="data" value="true" class="<?= FCB ?>"><?= $tm["mod"] ?></button>
        </form>

        <form class="<?= FC ?>" action="<?= FU ?>" method="post" enctype="multipart/form-data">
            <h2 class="<?= FCH ?>"><?= $tm["logo"] ?></h2>
            <img src="<?= $logo ?>" alt="Logo of your blog" height="64px" class="m-2" /><br>
            <input type="file" name="blogLogo" value="" accept="image/*" class="<?= FCI ?>">
            <button type="submit" name="data" value="true" class="<?= FCB ?>"><?= $tm["mod"] ?></button>
        </form>

        <form class="<?= FC ?>" action="<?= FU ?>" method="post" enctype="multipart/form-data">
            <h2 class="<?= FCH ?>"><?= $tm["ico"] ?></h2>
            <img src="<?= $icon ?>" alt="Icon of your blog" height="64px" class="m-2" /><br>
            <input type="file" name="blogIcon" value="" accept=".ico,.svg,.png" class="<?= FCI ?>">
            <button type="submit" name="data" value="true" class="<?= FCB ?>"><?= $tm["mod"] ?></button>
        </form>
    </div>
</main>
