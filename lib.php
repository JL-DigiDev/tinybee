<?php
    $bBal=array();
    $bBal[0]="@\[b\]@";
    $bBal[1]="@\[\/b\]@";
    $bBal[2]="@\[i\]@";
    $bBal[3]="@\[\/i\]@";
    $bBal[4]="@\[u\]@";
    $bBal[5]="@\[\/u\]@";
    $bBal[6]="@\[s\]@";
    $bBal[7]="@\[\/s\]@";
    $bBal[8]="@\[h3\]@";
    $bBal[9]="@\[\/h3\]@";
    $bBal[10]="@\[li\]@";
    $bBal[11]="@\[\/li\]@";
    $bBal[12]="@\[ul\]@";
    $bBal[13]="@\[\/ul\]@";
    $bBal[14]="@\[ol\]@";
    $bBal[15]="@\[\/ol\]@";
    $bBal[16]="@\[h1\]@";
    $bBal[17]="@\[\/h1\]@";
    $bBal[18]="@'\]@";
    $bBal[19]="@\[img url='@";
    $bBal[20]="@\[link url='@";
    $bBal[21]="@\[\/link\]@";

    $hBal=array();
    $hBal[0]="<b>";
    $hBal[1]="</b>";
    $hBal[2]="<i>";
    $hBal[3]="</i>";
    $hBal[4]="<u>";
    $hBal[5]="</u>";
    $hBal[6]="<s>";
    $hBal[7]="</s>";
    $hBal[8]="<h3>";
    $hBal[9]="</h3>";
    $hBal[10]="<li>";
    $hBal[11]="</li>";
    $hBal[12]="<ul>";
    $hBal[13]="</ul>";
    $hBal[14]="<ol>";
    $hBal[15]="</ol>";
    $hBal[16]="<h3>";
    $hBal[17]="</h3>";
    $hBal[18]="'>";
    $hBal[19]="<img src='";

    if(@$_GET["art"]){
        $hBal[20]="<a href='";
        $hBal[21]="</a>";
    }else{
        $hBal[20]="<span href='";
        $hBal[21]="</span>";
    }

    function dbCnx($dbHost,$dbPort="",$dbName,$dbUser,$dbPassword=""){
        try{
            $cnxDb = new PDO("mysql:host=".$dbHost.";port=".$dbPort.";dbname=".$dbName,$dbUser,$dbPassword);
            $cnxDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $cnxDb;
        }catch(Exception $e){
            return $e->getCode();
        }
    }

    function lang(){
        if(@$_SESSION["lang"]){
            if(@$_GET["l"]){
                $_SESSION["lang"]=htmlentities($_GET["l"]);
            }
        }else{
            $_SESSION["lang"]="fr";
        }
        session_encode();
    }

    function delDir($d){
        $dc=scandir($d);
        foreach ($dc as $key => $value) {
            if(!is_dir("$d/$value")){
                unlink("$d/$value");
            }
        }
        rmdir($d);
    }

    class GetDataFromDb{
        public $sql;
        public $data=array();

        function setSql($s=""){
            if(@$s){
                $this->sql=$s;
            }else{
                $this->sql="SELECT * FROM tb_data";
            }
        }

        function setData($d){
            $this->data=$d;
        }

        function getQuery($db){
            $r=$db->query($this->sql);
            $d=$r->fetch();
            $r->closeCursor();
            return $d;
        }

        function getMultiQuery($db){
            $r=$db->query($this->sql);
            $d=$r->fetchAll();
            $r->closeCursor();
            return $d;
        }

        function getPreparedQuery($db){
            $r=$db->prepare($this->sql);
            $r->execute($this->data);
            $d=$r->fetch();
            $r->closeCursor();
            return $d;
        }

        function getPreparedMultiQuery($db){
            $r=$db->prepare($this->sql);
            $r->execute($this->data);
            $d=$r->fetchAll();
            $r->closeCursor();
            return $d;
        }

        function sendData($db){
            $r=$db->prepare($this->sql);
            $r->execute($this->data);
            $r->closeCursor();
        }
    }

    class PicturesVerif{
        public $ext_valid=array("svg","png","jpg","jpeg","wbmp","bmp","gif");
        public $pic;
        public $dir;

        function setPic($p){
            $this->pic=$p;
        }

        function setDir($d){
            $this->dir=$d;
        }

        function loadPic(){
            $ext=pathinfo($this->pic["name"]);
            if(in_array(@$ext["extension"],$this->ext_valid)){
                $url=$this->dir."/".basename($this->pic["name"]);
                move_uploaded_file($this->pic["tmp_name"],$url);

                return $url;
            }
        }
    }

    class IconVerif extends PicturesVerif{
        public $ext_valid=array("svg","ico","png");
    }

    class GenPassword{
        public $password;

        function createPassword(){
            $this->password=dechex(mktime()*rand());
        }

        function getPassword(){
            return $this->password;
        }
    }
