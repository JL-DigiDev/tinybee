CREATE TABLE IF NOT EXISTS `tb_user`(
    `ID` INT PRIMARY KEY AUTO_INCREMENT,
    `Name` VARCHAR(255) NOT NULL,
    `Mail` VARCHAR(1023) UNIQUE NOT NULL,
    `Password` VARCHAR(255) NOT NULL,
    `Register` DATETIME NOT NULL DEFAULT NOW(),
    `IsActive` INT NOT NULL DEFAULT 1
);
CREATE TABLE IF NOT EXISTS `tb_cat`(
    `ID` INT PRIMARY KEY AUTO_INCREMENT,
    `Name` VARCHAR(255) NOT NULL UNIQUE,
    `IsActive` INT NOT NULL DEFAULT 1
);
CREATE TABLE IF NOT EXISTS `tb_art`(
    `ID` INT PRIMARY KEY AUTO_INCREMENT,
    `Name` VARCHAR(511) NOT NULL,
    `Content` TEXT NOT NULL,
    `Added` DATETIME NOT NULL DEFAULT NOW(),
    `CatID` INT NOT NULL,
    `UserID` INT NOT NULL,
    `Comment` INT NOT NULL DEFAULT 1,
    `IsActive` INT NOT NULL DEFAULT 1,
        FOREIGN KEY(`CatID`) REFERENCES `tb_cat`(`ID`),
        FOREIGN KEY(`UserID`) REFERENCES `tb_user`(`ID`)
);
CREATE TABLE IF NOT EXISTS `tb_comment`(
    `ID` INT PRIMARY KEY AUTO_INCREMENT,
    `Name` VARCHAR(255) NOT NULL,
    `Mail` VARCHAR(1023) NOT NULL,
    `IP` VARCHAR(127) NOT NULL,
    `Comment` TEXT NOT NULL,
    `IsActive` INT NOT NULL DEFAULT 1,
    `ArtID` INT NOT NULL,
    `Added` DATETIME NOT NULL DEFAULT NOW(),
        FOREIGN KEY(`ArtID`) REFERENCES `tb_art`(`ID`)
);
CREATE TABLE IF NOT EXISTS `tb_data`(
    `ID` VARCHAR(255) UNIQUE NOT NULL,
    `Val` VARCHAR(1023) NOT NULL
);


INSERT INTO tb_cat(Name) VALUES('Default');
INSERT INTO tb_data(ID) VALUES('blogIcon'),('blogLogo');
