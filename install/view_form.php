<!DOCTYPE html>
<html lang="<?= $_SESSION["lang"] ?>">
    <head>
        <meta charset="utf-8">
        <title><?= $n["title"] ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/svg" href="design/TinyBee-Icon.svg"/>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="scripts/bootstrap.min.js"></script>
        <link rel="stylesheet" href="design/bootstrap/bootstrap.min.css" />
    </head>
    <body>
        <main class="container-fluid">
            <div class="row">
                <form class="col-md m-5 p-2 border border-warning rounded" action="index.php" method="post" enctype="multipart/form-data">
                    <h1 class="text-center bg-warning rounded p-1"><img src="design/TinyBee-Icon.svg" alt="TinyBee Icon" height="64px" ><?= $n["title"] ?></h1>
                    <section class="alert alert-warning">
                        <?php require("admin/nav-lang.php"); ?>
                    </section>

                    <?php if(@$alertTop){ ?>
                    <section class="alert alert-danger">
                        <?= @$alertTop[$db] ?>
                    </section>
                    <?php } ?>

                    <h2 class="border-bottom border-warning"><?= $n["dTitle"] ?></h2>
                    <p class="alert alert-dark"><?= $n["dData"] ?></p>
                    <label for="dbHost" class="col-md-2"><?= $n["dHost"] ?> * : </label><input type="text" name="dbHost" value="" class="col-md-8 rounded border border-dark " required />
                    <label for="dbPort" class="col-md-2"><?= $n["dPort"] ?> : </label><input type="text" name="dbPort" value="" class="col-md-8 rounded border border-dark" />
                    <label for="dbName" class="col-md-2"><?= $n["dName"] ?> * : </label><input type="text" name="dbName" value="" class="col-md-8 rounded border border-dark" required />
                    <label for="dbUser" class="col-md-2"><?= $n["dUser"] ?> * : </label><input type="text" name="dbUser" value="" class="col-md-8 rounded border border-dark" required />
                    <label for="dbPassword" class="col-md-2"><?= $n["dPassword"] ?> : </label><input type="password" name="dbPassword" value="" class="col-md-8 rounded border border-dark" />

                    <h2 class="border-bottom border-warning"><?= $n["bTitle"] ?></h2>
                    <p class="alert alert-dark"><?= $n["bData"] ?></p>
                    <label for="blogName" class="col-md-2"><?= $n["bName"] ?> : </label><input type="text" name="blogName" value="" class="col-md-8 rounded border border-dark" required />
                    <label for="blogIcon" class="col-md-2"><?= $n["bIcon"] ?> : </label><input type="file" name="blogIcon" value="" class="col-md-8 rounded border border-dark" accept=".ico,.svg,.png" />
                    <label for="blogLogo" class="col-md-2"><?= $n["bLogo"] ?> : </label><input type="file" name="blogLogo" value="" class="col-md-8 rounded border border-dark" accept="image/*" />
                    <input type="hidden" name="blogLang" value="<?= $_SESSION["lang"] ?>">
                    <label for="blogTheme" class="col-md-2"><?= $n["bTheme"] ?> : </label>
                        <select class="col-md-8 rounded border border-dark" name="blogTheme">
                            <?= $theme ?>
                        </select>
                    <label for="blogComment" class="col-md-2"><?= $n["bCmt"] ?> : </label>
                        <select class="col-md-8 rounded border border-dark" name="blogComment">
                            <option value="1"><?= $n["bY"] ?></option>
                            <option value="0"><?= $n["bN"] ?></option>
                            <option value="2"><?= $n["bP"] ?></option>
                        </select>


                    <h2 class="border-bottom border-warning"><?= $n["uTitle"] ?></h2>
                    <p class="alert alert-dark"><?= $n["uData"] ?></p>
                    <label for="userName" class="col-md-2"><?= $n["uName"] ?> : </label><input type="text" name="userName" value="" class="col-md-8 rounded border border-dark" required />
                    <label for="userPassword" class="col-md-2"><?= $n["uPassword"] ?> : </label><input type="password" name="userPassword" value="" class="col-md-8 rounded border border-dark" required />
                    <label for="userMail" class="col-md-2"><?= $n["uMail"] ?> : </label><input type="email" name="userMail" value="" class="col-md-8 rounded border border-dark" required />

                    <button type="submit" name="submit" class="offset-md-4 col-md-2 btn btn-success"><img src="design/bootstrap-icon/patch-check.svg" alt="Submit image"> <?= $n["submit"] ?></button>
                    <button type="reset" name="reset" class="col-md-2 btn btn-danger"><img src="design/bootstrap-icon/trash.svg" alt="Reset image"> <?= $n["reset"] ?></button>

                </form>
            </div>
        </main>
    </body>
</html>
