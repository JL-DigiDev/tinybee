<?php
    lang();

    switch($_SESSION["lang"]){
        case "fr":
            $l=new FrenchForm();
            break;
        case "en":
            $l=new EnglishForm();
            break;
        case "nl":
            $l=new DutchForm();
            break;
    }

    $l->setForm();
    $n=$l->getForm();

    $cls="btn-warning";
    $uri="";

    ob_start();
    $tList=scandir("./design/css/");
    foreach ($tList as $key => $value) {
        if($key>1){
            $reg=explode(".",$value)[0];
            $name=explode("-",$reg)[1];
            ?>
                <option value="<?= $reg ?>"><?= $name ?></option>
            <?php
        }
    }
    $theme=ob_get_clean();
