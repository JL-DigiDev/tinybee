<?php
    function createDb($db){
        $sql=file_get_contents("install/tinybee-db.sql");
        $r=$db->exec($sql);
    }

    function insertUser($db,$n,$m,$p){
        $r_u=$db->prepare("INSERT INTO tb_user(Name,Mail,Password) VALUES(:n,:m,:p)");
        $r_u->execute(array("n"=>$n,"m"=>$m,"p"=>password_hash($p,PASSWORD_DEFAULT)));
        $r_u->closeCursor();
    }

    function insertBlog($db,$k,$v){
        $r_d=$db->prepare("INSERT INTO tb_data(ID,Val) VALUES(:i,:v)");
        $r_d->execute(array("i"=>$k,"v"=>$v));
        $r_d->closeCursor();
    }

    function insertArt($db){
        $n="First Post";
        $c="Hello, welcome on your TinyBee Blog.\r\nThis is an example post with an example category called 'Default'.\r\nIt's possible to remove there in your admin interface.\r\nHave a good day,\r\nThe TinyBee team.";
        $r_a=$db->prepare("INSERT INTO tb_art(Name,Content,CatID,UserID) VALUES(:Name,:Content,1,1)");
        $r_a->execute(array("Name"=>$n,"Content"=>$c));
        $r_a->closeCursor();
    }

    function createConf($dbHost,$dbPort="",$dbName,$dbUser,$dbPassword=""){
        $fileName="config.php";
        $txt="<?php\n\t// DB data\n\t\$dbHost=\"$dbHost\";\n\t\$dbPort=\"$dbPort\";\n\t\$dbName=\"$dbName\";\n\t\$dbUser=\"$dbUser\";\n\t\$dbPassword=\"$dbPassword\";\n?>";

        $f=fopen($fileName,"w");
        fwrite($f,$txt);
        fclose($f);
    }

    $valid=false;
    $f=array();
    $eDb=array(2002,1049,1045);
    $inf=array("blogName","blogIcon","blogLogo","blogTheme","blogComment","blogLang");
    foreach ($_POST as $key => $value) {
        $f[$key]=htmlentities($value);
    }
    $db=dbCnx($f["dbHost"],$f["dbPort"],$f["dbName"],$f["dbUser"],$f["dbPassword"]);
    if(is_numeric($db)&&in_array($db,$eDb)){
        switch ($_SESSION["lang"]) {
            case "fr":
                $a=new FrenchError();
                break;
            case "en":
                $a=new EnglishError();
                break;
            case "nl":
                $a=new DutchError();
                break;
        }
        $a->setError();
        $alertTop=$a->getError();
    }else{
        createDb($db);
        insertUser($db,$f["userName"],$f["userMail"],$f["userPassword"]);
        foreach ($inf as $key => $value) {
            if(@$value=="blogIcon"||@$value=="blogLogo"){
                $pic=($value=="blogIcon")? new IconVerif():new PicturesVerif();
                $pic->setPic($_FILES[$value]);
                $pic->setDir("design/usr");
                $d=$pic->loadPic();
            }else if(@$f[$value]){
                $d=$f[$value];
            }

            if(@$d){
                insertBlog($db,$value,$d);
            }
        }
        insertBlog($db,"blogMaster",1);
        insertArt($db);
        $valid=true;
        createConf($f["dbHost"],$f["dbPort"],$f["dbName"],$f["dbUser"],$f["dbPassword"]);

        switch ($_SESSION["lang"]) {
            case "fr":
                $s=new FrenchSuccess();
                break;
            case "en":
                $s=new EnglishSuccess();
                break;
            case "nl":
                $s=new DutchSuccess();
                break;
        }
        $s->setSuccess();
        $v=$s->getSuccess();
    }
