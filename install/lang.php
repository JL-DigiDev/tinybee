<?php
    class FrenchForm{
        public $form=array();

        function setForm(){
            $this->form["title"]="TinyBee Blog :: Installation";
            $this->form["lang"]="Français";
            $this->form["submit"]="Valider";
            $this->form["reset"]="Réinitialiser";

            // server text
            $this->form["dTitle"]="Paramètre serveur";
            $this->form["dData"]="Indiquer ici les informations relatives à la base de donnée de votre site.<br>Ces informations sont importantes, votre système ne pourra s'installer si vous n'avez pas ces informations ou si elles sont erronées...<br>Les champs noté d'une astérisque (*) sont obligatoires.";
            $this->form["dHost"]="Adresse de la Base de Donnée";
            $this->form["dPort"]="Port de l'adresse de la base de Donnée";
            $this->form["dName"]="Nom de la Base de Donnée";
            $this->form["dUser"]="Utilisateur de la Base de Donnée";
            $this->form["dPassword"]="Mot de Passe de la Base de Donnée";

            // blog text
            $this->form["bTitle"]="Paramètre du Blog";
            $this->form["bData"]="Indiquer ici les informations relatives à votre blog.<br>Les informations manquantes seront remplies par défault.<br>Ces informations pourront être modifiées plus tard.<br>La langue est automatiquement choisie suivant la langue d'installation.";
            $this->form["bName"]="Nom du blog";
            $this->form["bIcon"]="Icône du blog";
            $this->form["bLogo"]="Logo du blog";
            $this->form["bTheme"]="Thème (graphique) du blog";
            $this->form["bCmt"]="Commentaires";
            $this->form["bY"]="Activés";
            $this->form["bN"]="Désactivés";
            $this->form["bP"]="Activés avec modération";

            // User text
            $this->form["uTitle"]="Paramètre Utilisateur";
            $this->form["uData"]="Indiquer ici les informations à propos de l'administrateur du blog.<br>Ces informations peuvent être différentes des informations serveur.";
            $this->form["uName"]="Nom d'utilisateur";
            $this->form["uPassword"]="Mot de passe utilisateur";
            $this->form["uMail"]="Email utilisateur";
        }
        function getForm(){
            return $this->form;
        }
    }

    class EnglishForm extends FrenchForm{
        function setForm(){
            $this->form["title"]="TinyBee Blog :: Install";
            $this->form["lang"]="English";
            $this->form["submit"]="Submit";
            $this->form["reset"]="Reset";

            // Server text
            $this->form["dTitle"]="Server parameter";
            $this->form["dData"]="Write here the information about the database of your site<br>This information is important, your system don't install if you don't have this information or if it's wrong...<br>Fields with asterisk (*) is required.";
            $this->form["dHost"]="Database adress";
            $this->form["dPort"]="Database port";
            $this->form["dName"]="Database Name";
            $this->form["dUser"]="Database user";
            $this->form["dPassword"]="Database password";

            // blog text
            $this->form["bTitle"]="Blog parameter";
            $this->form["bData"]="Write here information about your blog.<br>The missing information will be filling by default.<br>This data may be modified later.<br>Langage is automatically chosen with the installation langage.";
            $this->form["bName"]="Blog name";
            $this->form["bIcon"]="Blog Icon";
            $this->form["bLogo"]="Blog Logo";
            $this->form["bTheme"]="Blog theme (graphic)";
            $this->form["bCmt"]="Comments";
            $this->form["bY"]="Enable";
            $this->form["bN"]="Disable";
            $this->form["bP"]="Enable with Pending";

            // User text
            $this->form["uTitle"]="User parameter";
            $this->form["uData"]="Write here information about the blog administrator.<br>This informations may be different from the server information.";
            $this->form["uName"]="User name";
            $this->form["uPassword"]="User password";
            $this->form["uMail"]="User email";
        }
    }

    class DutchForm extends FrenchForm{
        function setForm(){
            $this->form["title"]="TinyBee Blog :: Installeren";
            $this->form["lang"]="Nederlands";
            $this->form["submit"]="Sturen";
            $this->form["reset"]="Reset";

            // Server text
            $this->form["dTitle"]="Serverparameter";
            $this->form["dData"]="Schrijf hier de informatie over de database van uw site <br> Deze informatie is belangrijk, uw systeem installeert niet als u deze informatie niet heeft of als deze onjuist is ... <br> Velden met een asterisk (*) zijn verplicht .";
            $this->form["dHost"]="Database adres";
            $this->form["dPort"]="Databasepoort";
            $this->form["dName"]="Database Naam";
            $this->form["dUser"]="Database gebruiker";
            $this->form["dPassword"]="Database wachtwoord";

            // blog text
            $this->form["bTitle"]="Blog parameter";
            $this->form["bData"]="Schrijf hier informatie over je blog. <br> De ontbrekende informatie wordt standaard ingevuld. <br> Deze gegevens kunnen later worden gewijzigd. <br> Taal wordt automatisch gekozen bij de installatietaal.";
            $this->form["bName"]="Blog naam";
            $this->form["bIcon"]="Blog PictogramIcon";
            $this->form["bLogo"]="Blog Logo";
            $this->form["bTheme"]="Blogthema (grafisch)";
            $this->form["bCmt"]="opmerkingen";
            $this->form["bY"]="Inschakelen";
            $this->form["bN"]="Uitschakelen";
            $this->form["bP"]="inschakelen met in behandeling";

            // User text
            $this->form["uTitle"]="Gebruiker parameter";
            $this->form["uData"]="Schrijf hier informatie over de blogbeheerder. <be> Deze informatie kan afwijken van de serverinformatie.";
            $this->form["uName"]="Gebruikersnaam";
            $this->form["uPassword"]="Gebruikerswachtwoord";
            $this->form["uMail"]="Gebruiker e-mail";
        }
    }

    class FrenchError{
        public $error=array();
        public $ei="<img src=\"design/bootstrap-icon/exclamation-octogon.svg\" alt=\"Problem Icon\">";

        function setError(){
            $this->error[2002]="$this->ei Adresse de la base de donnée incorrecte (vérifier l'adresse et/ou le port)";
            $this->error[1049]="$this->ei Le nom de la base de donnée est incorrect";
            $this->error[1045]="$this->ei Le nom d'utilisateur et/ou le mot de passe de la base de donnée est incorrect";
        }
        function getError(){
            return $this->error;
        }
    }

    class EnglishError extends FrenchError{
        function setError(){
            $this->error[2002]="$this->ei Wrong Database Host (Verify host or port)";
            $this->error[1049]="$this->ei Wrong database name";
            $this->error[1045]="$this->ei Wrong user or password for database";
        }
    }

    class DutchError extends FrenchError{
        function setError(){
            $this->error[2002]="$this->ei Verkeerde databasehost (controleer host of poort)";
            $this->error[1049]="$this->ei Verkeerde databasenaam";
            $this->error[1045]="$this->ei Verkeerde gebruiker of wachtwoord voor database";
        }
    }

    class FrenchSuccess{
        public $success=array();

        function setSuccess(){
            $this->success["title"]="TinyBee :: Installation réussie";
            $this->success["congrat"]="Félicitation, votre blog TinyBee a été correctement installé!<br>Nous vous conseillons maintenant de vous rendre dans votre interface d'administration afin de débuter votre blog.";
            $this->success["home"]="Accueil";
            $this->success["admin"]="Admin";
        }
        function getSuccess(){
            return $this->success;
        }
    }

    class EnglishSuccess extends FrenchSuccess{
        function setSuccess(){
            $this->success["title"]="TinyBee :: Installation successfull";
            $this->success["congrat"]="Congratulation, your TinyBee blog was successfull installed!<br>We now advise you to go in your admin panel to start your blog.";
            $this->success["home"]="Home";
            $this->success["admin"]="Admin";
        }
    }

    class DutchSuccess extends FrenchSuccess{
        function setSuccess(){
            $this->success["title"]="TinyBee :: Installatie voltooid";
            $this->success["congrat"]="Gefeliciteerd, je TinyBee-blog is succesvol geïnstalleerd! <br> We raden je nu aan om naar je admin-paneel te gaan om je blog te starten.";
            $this->success["home"]="Ontvangst";
            $this->success["admin"]="Beheerder";
        }
    }
