<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title><?= $v["title"] ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/svg" href="design/TinyBee-Icon.svg"/>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="scripts/bootstrap.min.js"></script>
        <link rel="stylesheet" href="design/bootstrap/bootstrap.min.css" />
    </head>
    <body>
        <main class="container-fluid">
            <div class="row">
                <section class="col-md m-5 p-2 border border-warning rounded">
                    <h1 class="text-center bg-warning rounded p-1"><img src="design/TinyBee-Icon.svg" alt="TinyBee Icon" height="64px" ><?= $v["title"] ?></h1>
                    <p class="alert alert-success"><?= $v["congrat"] ?></p>
                    <div class="text-center">
                        <a href="?" class="btn btn-warning"><img src="design/bootstrap-icon/home.svg" alt="Home icon"> <?= $v["home"] ?></a>
                        <a href="?admin=c" class="btn btn-warning"><img src="design/bootstrap-icon/gear.svg" alt="Admin Gear icon"> <?= $v["admin"] ?></a>
                    </div>
                </section>
                <form  action="index.php" method="post" enctype="multipart/form-data">


            </div>
        </main>
    </body>
</html>
