<?php
    class BlogFrench{
        public $gen=array();
        public $cmt=array();

        // Setters
        function setGen(){
            $this->gen["prev"]="Plus récent";
            $this->gen["next"]="Plus ancien";
            $this->gen["noCmt"]="Les commentaires sont désactivés";
        }

        function setCmt(){
            $this->cmt["title"]="Commentaires";
            $this->cmt["empty"]="Aucun commentaire";
            $this->cmt["user"]="Nom";
            $this->cmt["mail"]="Email";
            $this->cmt["text"]="Votre commentaire";
            $this->cmt["pub"]="Poster un commentaire";
            $this->cmt["send"]="Commentaire envoyé";
        }

        // Getters
        function getGen(){
            return $this->gen;
        }
        function getCmt(){
            return $this->cmt;
        }
    }

    class BlogEnglish extends BlogFrench{
        function setGen(){
            $this->gen["prev"]="Most recent";
            $this->gen["next"]="Least recent";
            $this->gen["noCmt"]="Comment are disabled";
        }

        function setCmt(){
            $this->cmt["title"]="Comments";
            $this->cmt["empty"]="No comment";
            $this->cmt["user"]="Name";
            $this->cmt["mail"]="Email";
            $this->cmt["text"]="Your comment";
            $this->cmt["pub"]="Post comment";
            $this->cmt["send"]="Sending comment";
        }
    }

    class BlogDutch extends BlogFrench{
        function setGen(){
            $this->gen["prev"] = "Meest recent";
            $this->gen["next"] = "Minst recent";
            $this->gen["noCmt"] = "Reactie is uitgeschakeld";
        }

        function setCmt(){
            $this->cmt["title"] = "Reacties";
            $this->cmt["empty"] = "Geen commentaar";
            $this->cmt["user"] = "Naam";
            $this->cmt["mail"] = "E-mail";
            $this->cmt["text"] = "Jouw commentaar";
            $this->cmt["pub"] = "Plaats reactie";
            $this->cmt["send"] = "Verzenden commentaar";
        }
    }
