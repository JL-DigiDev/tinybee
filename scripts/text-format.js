let isBold=false,isItalic=false,isUnderLine=false,isStrike=false,isList=false,isOrderList=false;
let tTxt="",tUrl="",tLink="",tImg="",tList="";

function Format(t){
    const ca=document.getElementById("postContent").selectionStart;
    const pc=document.getElementById("postContent").value;
    const s1=pc.slice(0,ca);
    const s2=pc.slice(ca);
    const open="btn btn-outline-warning";
    const close="btn btn-warning";
    let m,isOpen;
    switch(t){
        case "b":
            isOpen=(isBold==false)?true:false;
            isBold=!isBold;
            break;
        case "i":
            isOpen=(isItalic==false)?true:false;
            isItalic=!isItalic;
            break;
        case "u":
            isOpen=(isUnderLine==false)?true:false;
            isUnderLine=!isUnderLine;
            break;
        case "s":
            isOpen=(isStrike==false)?true:false;
            isStrike=!isStrike;
            break;
    }
    if(isOpen==true){
        m="["+t+"]";
        document.getElementById("btn-"+t).classList.add("btn-outline-warning");
        document.getElementById("btn-"+t).classList.remove("btn-warning");
    }else{
        m="[/"+t+"]";
        document.getElementById("btn-"+t).classList.add("btn-warning");
        document.getElementById("btn-"+t).classList.remove("btn-outline-warning");
    }
    document.getElementById("postContent").value=s1+m+s2;
}

function Title(){
    if(tTxt==""){
        SetLang();
    }
    title=prompt(tTxt,"");
    document.getElementById("postContent").value+="[h1]"+title+"[/h1]";
}

function List(l){
    if(tTxt==""){
        SetLang();
    }
    isList=true;
    document.getElementById("postContent").value+="["+l+"]";
    while(isList==true){
        item=prompt(tList,"");
        if(item!=null){
            document.getElementById("postContent").value+="[li]"+item+"[/li]";
        }else{
            isList=false;
        }
    }
    document.getElementById("postContent").value+="[/"+l+"]";
}

function Picture(){
    if(tTxt==""){
        SetLang();
    }
    img=prompt(tImg,"");
    if(img!=null){
        document.getElementById("postContent").value+="[img url='"+img+"']";
    }
}

function Link(){
    if(tTxt==""){
        SetLang();
    }
    url=prompt(tUrl,"http://");
    link=prompt(tLink,"");
    if(url!=null){
        document.getElementById("postContent").value+=(link!=null)?"[link url='"+url+"']"+link+"[/link]":"[link url='"+url+"']"+url+"[/link]";
    }
}

// function

function SetLang(){
    lang=document.getElementById("lang").value;

    switch (lang) {
        case "fr":
            tTxt="Titre";
            tUrl="Adresse Web";
            tLink="Nom du lien";
            tImg="Lien de l'image";
            tList="Élément de liste";
            break;
        case "en":
            tTxt="Title";
            tUrl="Link address";
            tLink="Link name";
            tImg="Picture link";
            tList="List item";
            break;
        case "nl":
            tTxt="Titel";
            tUrl="Link adres";
            tLink="Link naam";
            tImg="Afbeelding link";
            tList="Lijstitem";
            break;
    }
}
